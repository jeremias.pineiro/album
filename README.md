Albumes 
Aplicacion para mostrar albunes de fotos.

...
**Clonar repositorio**

```
$ git clone https://gitlab.com/jeremias.pineiro/album.git
```

# verificacion y entrar al directorio

```
$ ls
```

```
$ cd album
```

**Crear imagen**

```
$ sudo docker build -t album-image .
```
**Desplegar contenedor**

```
$ sudo docker run -d -p 80:80 album-image
```
